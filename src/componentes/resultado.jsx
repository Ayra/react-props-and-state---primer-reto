import React, { Component } from 'react';

export default class Resultados extends Component {
    render() {
        function calcularMonto(anio, marca){
            const diffanio = 2019 - parseInt(anio);
            let montoBase = 2000;
            
            if(marca=="Americano") montoBase = 1.15*montoBase;
            else if(marca=="Europeo") montoBase = 1.3*montoBase;
            else if(marca == "Asiatico") montoBase = 1.05*montoBase;
            else montoBase = 0;

            let montoTotal = montoBase;

            for(let cont = 0 ; cont < diffanio; cont++){
                montoTotal = montoTotal - 0.03*montoTotal;
            }
            return montoTotal.toFixed(0);
        }
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-10 bg-info p-2">
                        COTIZAR
                </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-10 bg-info p-2 mt-1">
                        <h3>Resumen de Cotizacion</h3>
                        <div>Marca: {this.props.marca}</div>
                        <div>Plan: Completo</div>
                        <div>Año de auto: {this.props.anio}</div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-10 bg-info p-2 mt-1">
                        El total es : {calcularMonto(this.props.anio, this.props.marca)}
                </div>
                </div>
            </div>
        )
    }
}