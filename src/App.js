import React, { Component } from 'react';
import './App.css';

import Resultados from './componentes/resultado';
import { anios } from './anios.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Marca: "Europeo",
      Anio: "2008",
      anios: anios
     // Plan: [false, false]
    };

    this.handlerMarcaChange = this.handlerMarcaChange.bind(this);
    this.handlerAnioChange = this.handlerAnioChange.bind(this);
    //this.handlerBasicChange = this.handlerBasicChange.bind(this);
    //this.handlerCompleteChange = this.handlerCompleteChange.bind(this);
    fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(json => console.log(json))
  }

  handlerMarcaChange(event) {
    this.setState({
      Marca: event.target.value
    });
  }

  handlerAnioChange(event) {
    this.setState({
      Anio: event.target.value
    });
  }

 /* handlerBasicChange(event){
    this.setState({
      Plan: event.target.value
    });
  }*/

  

  render() {
    return (
      <div className="App">
        <div className="container col-md-6">
          <header>
            <h1 className="p-2 bg-info text-white mt-4">Cotizador de Seguro de Auto</h1>
          </header>
          <form className="col-md-10">
            <div className="form-row ml-5">
              <label className="my-1 mr-2" htmlFor="inlineFormCustomSelectPref">Marca</label>
              <select className="custom-select my-1 mr-sm-2"
                id="inlineFormCustomSelectPref"
                value={this.state.Marca}
                onChange={this.handlerMarcaChange}>
                <option value="Americano">Americano</option>
                <option value="Europeo">Europeo</option>
                <option value="Asiatico">Asiatico</option>
              </select>
            </div>
            <div className="form-row ml-5">
              <label className="my-1 mr-2" htmlFor="inlineFormCustomSelectPref2">Año</label>
              <select className="custom-select my-1 mr-sm-2"
                id="inlineFormCustomSelectPre2f"
                value={this.state.Anio}
                onChange={this.handlerAnioChange}>
                {
                  this.state.anios.map((anio, i)=>{
                    return (
                      <option value={anio.valor}>{anio.valor}</option>
                    );
                  })
                }
                
              </select>
            </div>
            <div className="form-row mt-2 ml-5">
              <div className="form-group row">
                <div className="col-md-2">Plan</div>
                <div className="col-md-auto">
                  <div className="form-check">
                    <input className="form-check-input"
                      type="radio"
                      id="gridCheck1"
                      name="radioCheck"
                      value="" />
                    <label className="form-check-label" htmlFor="gridCheck1">
                      Basico
                    </label>
                  </div>
                </div>
                <div className="col-md-auto">
                  <div className="form-check">
                    <input className="form-check-input"
                      type="radio" id="gridCheck2"
                      name="radioCheck" />
                    <label className="form-check-label" htmlFor="gridCheck2">
                      Completo
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <Resultados marca={this.state.Marca} anio={this.state.Anio}/>
        </div>
      </div>
    );
  }
}

export default App;
